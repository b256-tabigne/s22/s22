"use strict";

/*
    Create functions which can manipulate our arrays.
*/

let friend = [
    "Freddie Krugger",
    "Allen Walker",
    "Kira Yamato",
    "Tada Banri",
    "Sui Yamato",
    "Jonathan Cruz",
    "Yamato Nadeshiko"
];

let friendlist = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
const register = function(user) {

    if (friend.includes(user)) {

      alert("Registration failed. Username already exists!")

    } else {

      friend.push(user)

      alert("Thank you for registering!")
    }
  }

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
const addfriend = function(username){

    if(friend.includes(username)){

        friendlist.push(username)

        alert(`You have added ${username} as a friend!`)

    }else {

        alert("User not found.")
    }
}


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
const displayfriend = function(){

    if(friendlist.length == 0){

        alert("You currently have 0 friends. Add one first.")

    }else{
        for (let i = 0; i < friendlist.length; i++) {

            console.log(friendlist[i])

        }
    }
    
}


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


const zerofriend = function(){

    if(friendsList.length == 0){

        alert("You currently have 0 friends. Add one first.")

    }else{

        alert(`You currently have ${friendlist.length} friends.`)
    }
}

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
const deletefriends = function(){

    if(friendlist.length == 0){

        alert("You currently have 0 friends. Add one first.")

    }else{

        friendlist.pop()
    }
}

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
